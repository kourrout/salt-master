{% if grains['osfinger'] != 'Ubuntu-16.04' %}
always-fails:
  test.fail_without_changes:
    - name: "Requirement not met. Ubuntu 16 only"
    - failhard: True
{% endif %}

php-modules:
  pkg.installed:
    - pkgs:
      - apache2
      - php-gd
      - php-curl
      - php-cli
      - php-mbstring
      - php-zip
      - php-xml
      - libapache2-mod-php

/etc/apache2/sites-available/000-default.conf:
  file.managed:
    - source: salt://apps/grav/000-default.conf
    - user: root
    - group: root

mod-rewrite-module:
  apache_module.enabled:
    - name: rewrite

apache2:
  service:
    - running
    - enable: True
    - restart: True
    - watch:
      - file: /etc/apache2/sites-available/000-default.conf

/var/www:
  file.directory:
    - user: www-data
    - group: www-data

grav:
  git.latest:
    - name: https://github.com/getgrav/grav.git
    - target: /var/www/grav
    - user: www-data

grav-admin:
  cmd.run:
    - name: |
        bin/gpm -q install admin
        bin/plugin login newuser -u admin -p 123ABab__ -e admin@test.de -P b -N "Admin"
        bin/gpm -q install antimatter
        bin/gpm -q install learn2
    - runas: www-data
    - cwd: /var/www/grav
    - creates: /var/www/grav/user/plugins/admin
    - require:
      - git: grav

change-theme:
  file.replace:
    - name: /var/www/grav/user/config/system.yaml
    - pattern: 'theme: antimatter'
    - repl: 'theme: learn2'

place-a-welcome-page:
  file.managed:
    - name: /var/www/grav/user/pages/01.home/docs.md
    - source: salt://apps/grav/docs.md
    - user: www-data
    - group: www-data

remove-old-start-page:
  file.absent:
    - name: /var/www/grav/user/pages/01.home/default.md
