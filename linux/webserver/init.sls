# Install Apache

{% if grains['os'] == 'CentOS' %}
{% set name='httpd'%}
{% set user='apache'%}
{% else %}
{% set name='apache2' %}
{% set user='www-data'%}
{% endif %}



{{ name }}:
  pkg.installed: []
   

httpd-running:
  service.running:
    - name: {{ name }}
    - enable: true


{% for file in 'datei1.html', 'datei2.html','index.html' %}
/var/www/html/{{ file}}:
   file.managed:
     - contents: "<h1>Welcome to {{ grains['fqdn'] }}</h1>"
     - user: {{ user }}
     - group: {{ user }}
{% endfor %}

/var/www/html/content.html:
  file.managed:
    - source: salt://linux/webserver/content.html
    - template: jinja
    - user: {{ user }}
    - group: {{ user }}

