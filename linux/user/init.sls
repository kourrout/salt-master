group1:
  group.present:
    - name: www-data
    - system: false

barack:
  user.present:
    - fullname: Barack Obama 
    - shell: /usr/sbin/nologin
    - home: /var/barack
    - password: $1$k1sAhXqS$qg4vOab2pRwW8QTvPwg750
    - createhome: true
    - system: false
    - groups:
      - www-data
    - require:
      - group1
    - require_in:
      - baracks-key
    - failhard: true

baracks-key:
  ssh_auth.present:
    - user: barack
    - source: salt://linux/user/barack_id_rsa.pub
    - config: '%h/.ssh/authorized_keys'
    - require:
      - user: barack

welcome:
  file.managed:
    - name: ~barack/welcome.txt
    - owner: barack
    - group: www-data
    - content: Das file wurde von Saltstack kreiert!
