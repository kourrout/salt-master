tar-copy:
  archive.extracted:
    - name: /tmp/test
    - source: salt://linux/tarcopy/etc.tar.gz
    - archive_format: tar
    - user: www-data
    - group: www-data
    - if_missing: /tmp/test
