git:
  pkg.installed: []

download-bash-it:
  cmd.run:
    - name: git clone --depth=1 https://github.com/Bash-it/bash-it.git ~/.bash_it
    - unless: test -d .bash_it
    - runas: root
    - chdir: /root/
    - require:
      - pkg: git

install-bash-it:
  cmd.run:
    - name: .bash_it/install.sh -s
    - runas: root
    - chdir: /root/
    - creates: /root/.bashrc.bak
    - require:
       - cmd: download-bash-it

bash-rc:
  file.replace:
    - name: ~/.bashrc
    - pattern: export BASH_IT_THEME='bobby'        
    - repl: export BASH_IT_THEME='powerline'
    - prepend_if_not_found: true
