{% if grains['os'] == 'CentOS' %}
{% set name='java-1.8.0-openjdk-headless'%}
{% else %}
{% set name='default-jre-headless' %}
{% endif %}



{% if 0 != salt['cmd.retcode']('which java') %}
{{ name }}:
  pkg.installed: []
{% endif %}
