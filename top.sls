base:
  '*':
    - generic.welcome

  'kernel:linux':
     - match: grain
     - linux.user

  'os:windows':
     - match: grain
     - windows.user
