{% if pillar['software'] is defined %}
{% for key, value in pillar['software'].iteritems() %}
{{ key }}:
  pkg.installed: []
{% endfor %}
{% else %}
notice:
  test.succeed_without_changes:
    - name: "Nothing todo"
{% endif %}
